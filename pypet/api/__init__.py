from __future__ import absolute_import

# flake8: noqa

# import apis into api package
from pypet.api.another_fake_api import AnotherFakeApi
from pypet.api.default_api import DefaultApi
from pypet.api.fake_api import FakeApi
from pypet.api.fake_classname_tags123_api import FakeClassnameTags123Api
from pypet.api.pet_api import PetApi
from pypet.api.store_api import StoreApi
from pypet.api.user_api import UserApi
