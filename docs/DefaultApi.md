# pypet.DefaultApi

All URIs are relative to *http://petstore.swagger.io:80/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**foo_get**](DefaultApi.md#foo_get) | **GET** /foo | 


# **foo_get**
> FooGetDefaultResponse foo_get()



### Example

```python
from __future__ import print_function
import time
import os
import pypet
from pypet.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://petstore.swagger.io:80/v2
# See configuration.py for a list of all supported configuration parameters.
configuration = pypet.Configuration(
    host = "http://petstore.swagger.io:80/v2"
)


# Enter a context with an instance of the API client
with pypet.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = pypet.DefaultApi(api_client)

    try:
        api_response = api_instance.foo_get()
        print("The response of DefaultApi->foo_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->foo_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**FooGetDefaultResponse**](FooGetDefaultResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

